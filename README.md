# Proyecto Docker Compose

## Descripción

El objetivo es crear las imágenes y el manifiesto necesario ```docker-compose``` para levantar un stack de aplicación contruida en base a 4 Microservicios.

El proyecto BookInfo esta basado en un ejemplo de [Itsio](https://itsio.io) y utiliza 4 Microservicios escritos en distintos lenguajes:

* **Details (Ruby 2.3)**: Se comunica con Google y Amazon para obtener la información detallada de la publicación de un libro.
* **Ratings (NodeJS 4)**: Se conecta a una base de datos MongoDB para obtener el rating (numero de estrellas) de los reviews del libro.
* **Review** (Java IBM WebSphere): Entrega la lista de reviews de un libro. Si **Ratings** esta disponible imprime el rating del review.
* **ProductPage** (Python 2.7): Maneja la pagina de presentación, los usuarios y se comunica con el resto de los Microservicios para imprimir la información en pantalla.

Por otro lado, la capa de persistencia esta almacenada en una base de datos **MongoDB** la que posee una colección de ratings para cada review. 

```mermaid
graph LR;
  A[ProductPage] --> B[Reviews];
  A --> C[Details];
  B --> D>Ratings];
  D --> E(MongoDB);
```

## Repositorio

El proyecto y estas instrucciones se encuentran en https://gitlab.com/semoac/unab-compose-2018.


## Entregas

Cada alumno creará un Repositorio GIT privado en donde deberá:

* Hacer un fork de este proyecto.
* Dar acceso de lectura a semoac@gmail.com.
* Registrar cambios, idealmente, de manera progresiva.

## Evaluación

El esquema de evaluación será el siguiente:

Variable | Parámetro | Rango | Detalle
-----|-------------|------|---------
x | docker-compose levanta | 0, 1 | Debe ser 1 para optar a nota.
y | docker-compose levanta todo el stack y funciona | 1,2 | Solo algunos microservicios funcionan, todo parece funcionar
a | Dockerfile utiliza correctamente cache de docker | 0,1,2 | Ninguno, Algunos, Todos
b | Dockerfile presentan estrategias para disminuir el tamaño final de la imagen | 0,1,2 | Ninguno, Algunos, Todos
c | docker-compose maneja dependencias | 0,2 | No maneja, si Maneja.
d | docker-compose expone sólo un puerto en el servidor/laptop | 0,2 | No expone puertos o expone más de uno, expose sólo el necesario
f | Variables de entorno definidas con valores por omisión | 0,1,2 | Ninguno, Algunos, Todos
e | Se registraron avances de manera progresiva | 0,2 | Solo entrega final, se ven avances progresivos
g | Se reflejan los cambios de variables en docker-compose al momento de levantar la aplicación | 0,1,2 | Ninguno, Algunos, Todos

La formula del calculo de la nota del proyecto (NP) a partir de la nota de la entrega (NE) será:

```
NE = (a+b+c+d+e+f+g)/14 
NP = x*(5.0*(y/2) + 2.0*NE)
```

## Información técnica

### MongoDB

Se debe construir una imagen local basada en la imagen oficial de **MongoDB**. Esta imagen local deberá estar poblada con la siguiente información:
* Base de datos: ```test```.
* Collection: ```ratings```.
* Datos iniciales: El contenido del archivo ```ratings_data.json```.

**MongoDB**, al ser una base de datos *schemeless*, no necesita la definición previa de columnas, tablas o tipos de datos ya que estos se crean en tiempo de ejecución.

Para importar el archivo ```ratings_data.json``` a la base de datos se puede utilizar el siguiente comando dentro del contenedor:

```bash
mongoimport --host localhost --db test --collection ratings --drop --file /app/ratings_data.json
```

### Details

Este componente se comunicará con Google y Amazon para obtener la información detallada del libro. En caso que no tenga comunicación con los servicios externos imprimirá información falsa.

Las consideraciones para levantar este microservicio son las siguientes:

* Lenguaje: Ruby 2.3
* Ejecución: ```ruby details.rb 9080```
* Puerto de Red: ```9080```
* Nombre del contenedor: ```details```
* La variable de entorno ```ENABLE_EXTERNAL_BOOK_SERVICE``` puede tener los valores ```true``` o ```false``` y controla si  ```details``` debe utilizar los servicios externos para obtener la información del libro.

### Ratings

Este componente leerá de la base de datos **MongoDB** el rating de cada review. En caso que **MongoDB** no este disponible este componente imprimirá ratings falsos.

Las consideraciones para levantar este microservicio son las siguientes:

* Lenguaje: Node 4.
* Ejecución: ```node /app/ratings.js 9080```
* Puerto de Red: ```9080```.
* * Nombre del contenedor: ```ratings```
* La variable de entorno ```SERVICE_VERSION``` puede tener los valores ```v1``` o ```v2``` y controla si debe imprimir ratings falsos (```v1```) o si debe conectarse a la base de datos **MongoDB** (```v2```).
* La variable de entorno ```MONGO_DB_URL``` almacena la url de conexión a **MongoDB**. Para consumir de forma correcta desde un contenedor llamado ```mongodb``` debe tener el valor ```mongodb://mongodb:27017/test```.


### Reviews

Este Microservicio esta escrito en Java utilizando el servidor de aplicaciones IBM Websphere.
La imagen de este servicio ya esta contruida. Se debe utilizar al imagen ```semoac/reviews```.

Las consideraciones para levantar este microservicio son las siguientes:

* Lenguaje: Java en Webshere Liberty.
* Puerto de Red: ```9080```.
* Nombre del contenedor: ```reviews```
* La variable de entorno ```ENABLE_RATINGS```puede tener los valores ```true``` o ```false``` y controla si ```reviews``` debe comunicarse con el microservicio ```ratings```.


### ProductPage

Este componente se encarga de publicar la capa de presentación de la aplicación web y se comunica con el resto de los microservicios para obtener la información de un libro. Este es el unico contenedor que debe ser expuesto al exterior.

Las consideraciones para levantar este microservicio son las siguientes:

* Lenguaje: Python 2.7
* Ejecución: ```python productpage.py 9080```.
* Puerto de Red: ```9080```.
* Nombre del contenedor: ```productpage```
* La variable de entorno ```ENABLE_RATINGS```puede tener los valores ```TRUE``` o ```FALSE``` y controla si ```reviews``` debe comunicarse con el microservicio ```ratings```.


